/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.stax.service;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

/**
 * Interface defining a <tt>StAXFactoryService</tt>
 * which provides access to StAX factory instances.
 *
 * @author Dieter Wimberger
 * @version 1.0
 */
public interface StAXFactoryService {

  /**
   * Returns an <tt>XMLInputFactory</tt>.
   *
   * @return an <tt>XMLInputFactory</tt> instance.
   */
  public XMLInputFactory getXMLInputFactory();

  /**
   * Returns an <tt>XMLOutputFactory</tt>.
   *
   * @return an <tt>XMLOutputFactory</tt> instance.
   */
  public XMLOutputFactory getXMLOutputFactory();

  /**
   * Returns an <tt>XMLEventFactory</tt>.
   *
   * @return an <tt>XMLEventFactory</tt> instance.
   */
  public XMLEventFactory getXMLEventFactory();

}//interface StAXFactoryService