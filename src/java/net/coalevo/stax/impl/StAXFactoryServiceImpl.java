/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.stax.impl;


import com.ctc.wstx.stax.WstxEventFactory;
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;
import net.coalevo.stax.service.StAXFactoryService;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

/**
 * Provides an implementation for {@link StAXFactoryService}.
 *
 * @author Dieter Wimberger
 * @version 1.0
 */
class StAXFactoryServiceImpl implements StAXFactoryService {

  private static ClassLoader c_ClassLoader;

  public StAXFactoryServiceImpl() {
    c_ClassLoader = getClass().getClassLoader();
  }//constructor

  public XMLInputFactory getXMLInputFactory() {
    final ClassLoader cl = Thread.currentThread().getContextClassLoader();
    Thread.currentThread().setContextClassLoader(c_ClassLoader);
    try {
      return new WstxInputFactory();
    } finally {
      Thread.currentThread().setContextClassLoader(cl);
    }
  }//getXMLInputFactory

  public XMLOutputFactory getXMLOutputFactory() {
    final ClassLoader cl = Thread.currentThread().getContextClassLoader();
    Thread.currentThread().setContextClassLoader(c_ClassLoader);
    try {
      return new WstxOutputFactory();
    } finally {
      Thread.currentThread().setContextClassLoader(cl);
    }
  }//getXMLOutputFactory

  public XMLEventFactory getXMLEventFactory() {
    final ClassLoader cl = Thread.currentThread().getContextClassLoader();
    Thread.currentThread().setContextClassLoader(c_ClassLoader);
    try {
      return new WstxEventFactory();
    } finally {
      Thread.currentThread().setContextClassLoader(cl);
    }
  }//getXMLEventFactory

}//interface StAXFactoryService